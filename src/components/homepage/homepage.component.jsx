import React from 'react';
import CategoryList from '../categoryList/categoryList.component';

const Homepage = () => (
  <div className='homepage'>
    <CategoryList />
  </div>
);

export default Homepage;