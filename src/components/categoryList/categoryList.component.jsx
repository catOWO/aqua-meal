import React from 'react';
import { connect } from 'react-redux';

import Meal from '../category/catergory.component';
import './categoryList.styles.scss';

const CategoryList = ({ categories }) => (
  <ul className='meal-category-list'>
    { categories && categories.length &&
      categories.map(category => <Meal key={category.idCategory} {...category} />)}
  </ul>
);

const mapStateToProps = state => {
  const { categories } = state.meals;
  return { categories };
}

export default connect(mapStateToProps)(CategoryList);