import React from 'react';
import { connect } from 'react-redux';

import { getCategoryMeals } from '../../redux/reducers/categorySlice';

import './category.styles.scss';

const Category = ({ strCategory, strCategoryThumb, strCategoryDescription, getCategoryMeals }) => {
  const handleClickCategory = ({ target }) => {
    const { id } = target;

    getCategoryMeals(id);
  }

  return (
    <li className='li-meal-category'>
      <div className='meal-category' id={strCategory} onClick={ handleClickCategory }>
        <img src={`${strCategoryThumb}`} alt={strCategory} id={strCategory}/>
        <div className='meal-category-info' id={strCategory}>
          <span className='meal-category-title'>
            { strCategory }
            <br/>
            <span className='meal-category-description'>
              { strCategoryDescription }
            </span>
          </span>
        </div>
      </div>
    </li>
  );
}

export default connect(null, { getCategoryMeals })(Category);