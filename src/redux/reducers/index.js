import { combineReducers } from 'redux';

import categoryReducer from './categorySlice';

const rootReducer = combineReducers({
  meals: categoryReducer
});

export default rootReducer;