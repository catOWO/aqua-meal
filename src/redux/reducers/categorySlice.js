import mealDBClient from "../../api/mealDBClient";
import { CATEGORIES_LOADED, CATEGORY_SELECTED } from "../actionTypes";

const initialState = {};

export default function categoryReducer(state = initialState, action) {
  switch (action.type) {
    case CATEGORIES_LOADED: {
      return {
        ...state,
        categories: action.payload
      };
    }
    case CATEGORY_SELECTED: {
      return {
        ...state,
        meals: action.payload
      };
    }
    default:
      return state;
  }
}

export async function getCategories(dispatch, getState) {
  const { categories } = await mealDBClient.getCategories();
  dispatch({ type: CATEGORIES_LOADED, payload: categories });
}

export function getCategoryMeals(categoryName) {
  return async function getMealsThunk(dispatch, getState) {
    const { meals } = await mealDBClient.getCategoryMeals(categoryName);
    console.log(meals);
    dispatch({ type: CATEGORY_SELECTED, payload: meals });
  }
}