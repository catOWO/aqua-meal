import axios from "axios";

const BASE_ULR = 'https://www.themealdb.com/api/json';
const VERSION = 'v1';
const KEY = '1';

class MealDBClient {
  #client = null;
  constructor() {
    this.#client = axios.create({
      baseURL: `${BASE_ULR}/${VERSION}/${KEY}/`,
      responseType: "json"
    });
  }

  async getCategories() {
    let categoriesInfo;
    try {
      const response = await this.#client.get('categories.php');
      categoriesInfo = response.data;
      if (response.status === 200) {
        return categoriesInfo;
      }
      throw new Error(response.statusText);
    } catch (error) {
      return Promise.reject(error.message ? error.message : categoriesInfo);
    }
  }

  async getCategoryMeals(categoryName) {
    let categoryData;
    try {
      const response = await this.#client.get(`filter.php?c=${categoryName}`);
      categoryData = response.data;
      if (response.status === 200) {
        return categoryData;
      }
      throw new Error(response.statusText);
    } catch (error) {
      return Promise.reject(error.message ? error.message : categoryData);
    }
  }
}

export default MealDBClient = new MealDBClient();